# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('all_page', '0011_auto_20160728_1859'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserMail',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200, null=True, blank=True)),
                ('correo', models.CharField(max_length=200, null=True, blank=True)),
                ('mensaje', models.CharField(max_length=1500, null=True, blank=True)),
            ],
            options={
                'ordering': ['name'],
            },
        ),
        migrations.AlterModelOptions(
            name='ingredients',
            options={'ordering': ['name']},
        ),
    ]

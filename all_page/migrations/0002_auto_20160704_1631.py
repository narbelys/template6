# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('all_page', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='recipes',
            name='light',
            field=models.IntegerField(default=0, null=True),
        ),
        migrations.AlterField(
            model_name='recipes',
            name='category',
            field=models.CharField(max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name='recipes',
            name='content',
            field=models.TextField(max_length=1500),
        ),
        migrations.AlterField(
            model_name='recipes',
            name='creator',
            field=models.CharField(max_length=1500, null=True),
        ),
        migrations.AlterField(
            model_name='recipes',
            name='pais',
            field=models.CharField(max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name='recipes',
            name='plato',
            field=models.CharField(max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name='recipes',
            name='tiempo',
            field=models.IntegerField(default=1, null=True),
        ),
    ]

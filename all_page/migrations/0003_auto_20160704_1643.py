# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('all_page', '0002_auto_20160704_1631'),
    ]

    operations = [
        migrations.AddField(
            model_name='recipes',
            name='imagen_principal',
            field=models.TextField(max_length=1500, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='ingredientsrecipes',
            name='cantidad',
            field=models.CharField(max_length=1500, blank=True),
        ),
        migrations.AlterField(
            model_name='recipes',
            name='category',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='recipes',
            name='creator',
            field=models.CharField(max_length=1500, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='recipes',
            name='light',
            field=models.IntegerField(default=0, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='recipes',
            name='pais',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='recipes',
            name='plato',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='recipes',
            name='tiempo',
            field=models.IntegerField(default=1, null=True, blank=True),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('all_page', '0004_auto_20160705_1513'),
    ]

    operations = [
        migrations.AlterField(
            model_name='recipes',
            name='category',
            field=models.ForeignKey(blank=True, to='all_page.Category', null=True),
        ),
        migrations.AlterField(
            model_name='recipes',
            name='pais',
            field=models.ForeignKey(blank=True, to='all_page.Country', null=True),
        ),
        migrations.AlterField(
            model_name='recipes',
            name='plato',
            field=models.ForeignKey(blank=True, to='all_page.Food', null=True),
        ),
    ]

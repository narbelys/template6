# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('all_page', '0010_auto_20160728_1856'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ingredientsrecipes',
            name='ingredients',
            field=models.ForeignKey(blank=True, to='all_page.Ingredients', null=True),
        ),
    ]

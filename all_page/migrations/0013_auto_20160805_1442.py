# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('all_page', '0012_auto_20160804_2053'),
    ]

    operations = [
        migrations.CreateModel(
            name='RecipesNotFound',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=1500, null=True, blank=True)),
                ('ingredientes', models.CharField(max_length=1500, null=True, blank=True)),
            ],
            options={
                'ordering': ['name'],
            },
        ),
        migrations.AlterModelOptions(
            name='usermail',
            options={'ordering': ['correo']},
        ),
    ]

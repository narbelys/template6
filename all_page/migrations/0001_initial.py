# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Ingredients',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='IngredientsRecipes',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cantidad', models.CharField(max_length=1500)),
                ('ingredients', models.ForeignKey(to='all_page.Ingredients')),
            ],
        ),
        migrations.CreateModel(
            name='Recipes',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('content', models.CharField(max_length=1500)),
                ('creator', models.CharField(max_length=1500)),
                ('porciones', models.IntegerField(default=1)),
                ('pais', models.CharField(max_length=200)),
                ('plato', models.CharField(max_length=200)),
                ('category', models.CharField(max_length=200)),
                ('tiempo', models.IntegerField(default=1)),
            ],
        ),
        migrations.AddField(
            model_name='ingredientsrecipes',
            name='recipes',
            field=models.ForeignKey(to='all_page.Recipes'),
        ),
    ]

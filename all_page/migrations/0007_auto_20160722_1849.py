# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('all_page', '0006_recipes_home'),
    ]

    operations = [
        migrations.CreateModel(
            name='Tips',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('imagen', models.CharField(max_length=1500, null=True, blank=True)),
                ('content', models.TextField(max_length=40000)),
                ('creator', models.CharField(max_length=1500, null=True, blank=True)),
            ],
        ),
        migrations.AlterField(
            model_name='recipes',
            name='content',
            field=models.TextField(max_length=40000),
        ),
    ]

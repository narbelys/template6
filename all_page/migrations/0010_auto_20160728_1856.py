# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('all_page', '0009_auto_20160725_2136'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ingredientsrecipes',
            name='ingredients',
            field=models.ForeignKey(to='all_page.Ingredients', blank=True),
        ),
    ]

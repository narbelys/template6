# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('all_page', '0013_auto_20160805_1442'),
    ]

    operations = [
        migrations.AlterField(
            model_name='recipes',
            name='content',
            field=tinymce.models.HTMLField(max_length=40000, blank=True),
        ),
    ]

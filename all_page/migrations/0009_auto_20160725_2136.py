# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('all_page', '0008_tips_description'),
    ]

    operations = [
        migrations.AlterField(
            model_name='recipes',
            name='content',
            field=models.TextField(max_length=40000, blank=True),
        ),
    ]

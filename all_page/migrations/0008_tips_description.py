# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('all_page', '0007_auto_20160722_1849'),
    ]

    operations = [
        migrations.AddField(
            model_name='tips',
            name='description',
            field=models.CharField(max_length=1500, null=True, blank=True),
        ),
    ]

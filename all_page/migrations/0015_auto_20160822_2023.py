# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('all_page', '0014_auto_20160822_2021'),
    ]

    operations = [
        migrations.AlterField(
            model_name='recipes',
            name='content',
            field=tinymce.models.HTMLField(),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('all_page', '0005_auto_20160705_1526'),
    ]

    operations = [
        migrations.AddField(
            model_name='recipes',
            name='home',
            field=models.IntegerField(default=0, null=True, blank=True),
        ),
    ]

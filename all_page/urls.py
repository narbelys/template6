from django.conf.urls import include, url
from . import views

urlpatterns = [
    url(r'^$', views.recetas, name='recetas'),
    url(r'^recetas/', views.recetas, name='recetas'),
    url(r'^recetas/(?P<page>[0-9]+)/$', views.recetas, name='recetas'),
    url(r'^type_recetas/(?P<page>[0-9]+)/(?P<type_name>\w+)/$', views.type_recetas, name='type_recetas'),
    url(r'^detalles/(?P<recetas_id>[0-9]+)/$', views.detalle, name='detalle'),    
    url(r'^tips/', views.tips, name='tips'),
    url(r'^script/', views.script, name='script'),
    url(r'^detalles_tips/(?P<tips_id>[0-9]+)/$', views.detalle_tips, name='detalle_tips'),
    url(r'^template/', views.index, name='index'),
    url(r'^nosotros/', views.nosotros, name='nosotros'),
    url(r'^contacto/', views.contacto, name='contacto'),
    url(r'^politicas/', views.politicas, name='politicas'),
    url(r'^tinymce/', include('tinymce.urls')),
]

#!/usr/bin/env python
#coding: utf8 
import os

from django.shortcuts import render

# Create your views here.


from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from django.shortcuts import get_object_or_404, render_to_response, redirect
from django.template import RequestContext
from django.contrib.auth import authenticate
from django.http import HttpResponseRedirect, HttpResponse,\
                        HttpResponseNotAllowed, HttpResponseBadRequest, \
                        HttpResponseForbidden
        
from django.core.mail import send_mail
from django.conf import settings
from django.db.models import Q
from django.core.paginator import Paginator
from django.contrib.sessions.backends.db import SessionStore
from all_page.models import Recipes, IngredientsRecipes, Ingredients, Country, Food, Category, Tips, UserMail, RecipesNotFound

def index(request):
    template_name = 'index.html'
    return render_to_response(
		template_name, 
		{}, 
		context_instance=RequestContext(request)
	)
def contacto(request):
    template_name = 'contacto.html'
    return render_to_response(
		template_name, 
		{}, 
		context_instance=RequestContext(request))
def nosotros(request):
    template_name = 'nosotros.html'
    return render_to_response(
		template_name, 
		{}, 
		context_instance=RequestContext(request))
def politicas(request):
    template_name = 'politicas.html'
    return render_to_response(
		template_name, 
		{}, 
		context_instance=RequestContext(request))

def recetas(request, page = None):
    template_name = 'home.html'
    data={}
    data["header"]='home'
    #114 crema
    #aux=76
    #while aux > 0:
    #    ingredientsRecipes = IngredientsRecipes.objects.filter(recipes_id=aux)
    #    for i in ingredientsRecipes:
    #        i.cantidad=i.cantidad + ' ' + i.ingredients.name
            #i.save()
    #    aux =aux-1
    
    if not page:
        page = 0
        
    
    if request.method == 'GET':
        recipes = Recipes.objects.all()
        data["country"] = Country.objects.all()
        data["food"] = Food.objects.all()
        data["category"] = Category.objects.all()
        data["recipes_random"] = Recipes.objects.order_by('?')[:5]
        data["ingredients"] = Ingredients.objects.all()
        data["recipes_home"] = Recipes.objects.filter(home=True)[:6]
        data["recipes"]=[]
        data["post"]=False
        #for i in recipes: 
        #    data["recipes"]=[{'recipe_object':i, 'ingredients_active': IngredientsRecipes.objects.filter(recipes=i), 'ingredients_inactive': IngredientsRecipes.objects.filter(recipes=i)}]+data["recipes"]
         
        #del request.session['recipes']
        
        if 'recipes' in request.session:
            
            page = request.GET.get('page', None)
            if not page:
                del request.session['recipes']
            else:
                data2 = request.session['recipes']
                data2["recipes"]= request.session['paginador'].page(page)
                return render_to_response(
                template_name, 
                data2, 
		context_instance=RequestContext(request))
            
            
        
        return render_to_response(
            template_name, 
            data, 
		context_instance=RequestContext(request))
    elif request.method == 'POST':
        print request.POST
        category = request.POST.get('category',None)
        name = request.POST.get('name',None)
        ingredients = request.POST.get('ingredients',None)
        food = request.POST.get('food',None)
        country = request.POST.get('country',None)
        low = request.POST.get('low',None)
        mensaje_s = request.POST.get('mensaje_s',None)
        name_s = request.POST.get('name_s',None)
        correo_s = request.POST.get('correo_s',None)
        
        if correo_s: 
            user= UserMail(name=name_s, correo=correo_s, mensaje=mensaje_s)
            user.save()
            recipes = Recipes.objects.all()
            data["country"] = Country.objects.all()
            data["food"] = Food.objects.all()
            data["category"] = Category.objects.all()
            data["recipes_random"] = Recipes.objects.order_by('?')[:5]
            data["ingredients"] = Ingredients.objects.all()
            data["recipes_home"] = Recipes.objects.filter(home=True)[:6]
            data["recipes"]=[]
            data["post"]=False
            data["aceptado"]=True
            return render_to_response(
                template_name, 
                data, 
		context_instance=RequestContext(request))
        
        data["form_category"] = category
        data["form_name"] = name
        data["form_ingredients"] = ingredients
        data["form_food"] = food
        data["form_country"] = country
        data["form_low"] = low
        condicion = Q()
        if category:
            condicion = condicion & Q(category_id=category)  
            data["form_category"] = int(data["form_category"])
            
        if food:
            condicion = condicion & Q(plato_id=food)   
            data["form_food"] = int(data["form_food"])
        if name:
            condicion = condicion & Q(name__contains=name)        
        if country:
            condicion = condicion & Q(pais_id=country)    
            data["form_country"] = int(data["form_country"])
        if low:
            condicion = condicion & Q(light=True)
          
        ingredients_array = ingredients.split(', ')
        ingredient_recipes= IngredientsRecipes.objects.filter(ingredients__name__in = ingredients_array)
        #.exclude(ingredients__isnull=True).distinct()
        recipes_values = ingredient_recipes.values_list('recipes', flat=True).distinct()
        if ingredients: 
            print 'array', ingredients_array
            condicion = condicion & Q(id__in=recipes_values)
        
            
        print condicion   
        recipes = Recipes.objects.filter(condicion)
        
        if not recipes:
            notfound=RecipesNotFound(name=name, ingredientes=ingredients)
            notfound.save()
            data["country"] = Country.objects.all()
            data["food"] = Food.objects.all()
            data["category"] = Category.objects.all()
            data["recipes_random"] = Recipes.objects.order_by('?')[:5]
            data["ingredients"] = Ingredients.objects.all()
            data["recipes_home"] = Recipes.objects.filter(home=True)[:6]
        
        
            data['notfound']='Receta no encontrada, evaluaremos su futura incorporacion. Por favor suscribete para enviarte nuestras recetas incorporadas.'
            return render_to_response(
                template_name, 
                data, 
                context_instance=RequestContext(request)
            )
        #print recipes
        
        data["country"] = Country.objects.all()
        data["food"] = Food.objects.all()
        data["category"] = Category.objects.all()
        data["recipes_random"] = Recipes.objects.order_by('?')[:5]
        data["ingredients"] = Ingredients.objects.all()
        data["recipes_home"] = Recipes.objects.filter(home=True)[:6]
        data["recipes"]=[]
        data["post"]=True
        for i in recipes: 
            if ingredients:
                active= IngredientsRecipes.objects.filter(recipes=i, ingredients__name__in = ingredients_array).exclude(ingredients__isnull=True).values('ingredients__name').distinct()
                inactive = IngredientsRecipes.objects.filter(recipes=i).exclude(ingredients__name__in = ingredients_array).exclude(ingredients__isnull=True).values('ingredients__name').distinct()
                
                data["recipes"]=[{'recipe_object':i, 'ingredients_active': active, 'ingredients_inactive': inactive , 'cantidad': active.count() }]+data["recipes"]
            else:      
                active=IngredientsRecipes.objects.filter(recipes=i).exclude(ingredients__isnull=True).values('ingredients__name').distinct()
                
                data["recipes"]=[{'recipe_object':i, 'ingredients_active': active, 'ingredients_inactive': [] , 'cantidad': active.count()}]+data["recipes"]
            

        init= page*10
        data["recipes"] = sorted(data["recipes"], key=lambda k: k['cantidad'], reverse=True)
        data_save = data
        request.session['recipes']= data_save  
         
        #print '---->', data_save

        
        paginator = Paginator(data["recipes"], 10)
        data["recipes"]= paginator.page(1)
        request.session['paginador']= paginator 
        #print data["recipes"]
        #count=data["recipes"].count()
        #data["recipes"] = data["recipes"][init:init+10]
        #i=0
        #j=0
        #pages=[]
        #print count
        #while i<count:
        #    pages.push(j)
        #    j = j+1
        #    i= i+10
        return render_to_response(
            template_name, 
            data, 
		context_instance=RequestContext(request))
        
def type_recetas(request, page = None, type_name = None):
    template_name = 'pages.html'
    data={}
    data["header"]='home'
    #114 crema
    #aux=76
    #while aux > 0:
    #    ingredientsRecipes = IngredientsRecipes.objects.filter(recipes_id=aux)
    #    for i in ingredientsRecipes:
    #        i.cantidad=i.cantidad + ' ' + i.ingredients.name
            #i.save()
    #    aux =aux-1
    
    
    print '----->', page
    
    if not page:
        page = 0
    
    if request.method == 'GET':
        #categoria=Category.objects.get(name=type_name)
        if type_name=='Light':
            recipes = Recipes.objects.filter(light=True)
        elif type_name=='divertidos':
            recipes = Recipes.objects.filter(category__name=u'Diseños divertidos')
        else:
            recipes = Recipes.objects.filter(category__name=type_name)
        #recipes = Recipes.objects.all()
        data["country"] = Country.objects.all()
        data["food"] = Food.objects.all()
        data["category"] = Category.objects.all()
        data["recipes_random"] = Recipes.objects.order_by('?')[:5]
        data["ingredients"] = Ingredients.objects.all()
        data["recipes_home"] = Recipes.objects.filter(home=True)[:6]
        data["recipes"]=[]
        data["post"]=False
        data["type_name"]=type_name
        #for i in recipes: 
        #    data["recipes"]=[{'recipe_object':i, 'ingredients_active': IngredientsRecipes.objects.filter(recipes=i), 'ingredients_inactive': IngredientsRecipes.objects.filter(recipes=i)}]+data["recipes"]
         
        #del request.session['recipes']
        print recipes,data["category"]
        ingredients_array=[]
        for i in recipes:  
            active=IngredientsRecipes.objects.filter(recipes=i).exclude(ingredients__isnull=True).values('ingredients__name').distinct()

            data["recipes"]=[{'recipe_object':i, 'ingredients_active': active, 'ingredients_inactive': [] , 'cantidad': active.count()}]+data["recipes"]
            

        data["recipes"] = sorted(data["recipes"], key=lambda k: k['cantidad'], reverse=True)
        data_save = data
        request.session['recipes']= data_save  
         
        #print '---->', data_save

        
        paginator = Paginator(data["recipes"], 10)
        data["recipes"]= paginator.page(page)
        request.session['paginador']= paginator
            
            
        return render_to_response(
            template_name, 
            data, 
		context_instance=RequestContext(request))
    elif request.method == 'POST':
        print request.POST
        category = request.POST.get('category',None)
        name = request.POST.get('name',None)
        ingredients = request.POST.get('ingredients',None)
        food = request.POST.get('food',None)
        country = request.POST.get('country',None)
        low = request.POST.get('low',None)
        mensaje_s = request.POST.get('mensaje_s',None)
        name_s = request.POST.get('name_s',None)
        correo_s = request.POST.get('correo_s',None)
        
        if correo_s: 
            user= UserMail(name=name_s, correo=correo_s, mensaje=mensaje_s)
            user.save()
            recipes = Recipes.objects.all()
            data["country"] = Country.objects.all()
            data["food"] = Food.objects.all()
            data["category"] = Category.objects.all()
            data["recipes_random"] = Recipes.objects.order_by('?')[:5]
            data["ingredients"] = Ingredients.objects.all()
            data["recipes_home"] = Recipes.objects.filter(home=True)[:6]
            data["recipes"]=[]
            data["post"]=False
            data["aceptado"]=True
            return render_to_response(
                template_name, 
                data, 
		context_instance=RequestContext(request))
        
        data["form_category"] = category
        data["form_name"] = name
        data["form_ingredients"] = ingredients
        data["form_food"] = food
        data["form_country"] = country
        data["form_low"] = low
        condicion = Q()
        if category:
            condicion = condicion & Q(category_id=category)  
            data["form_category"] = int(data["form_category"])
            
        if food:
            condicion = condicion & Q(plato_id=food)   
            data["form_food"] = int(data["form_food"])
        if name:
            condicion = condicion & Q(name__contains=name)        
        if country:
            condicion = condicion & Q(pais_id=country)    
            data["form_country"] = int(data["form_country"])
        if low:
            condicion = condicion & Q(light=True)
          
        ingredients_array = ingredients.split(', ')
        ingredient_recipes= IngredientsRecipes.objects.filter(ingredients__name__in = ingredients_array)
        #.exclude(ingredients__isnull=True).distinct()
        recipes_values = ingredient_recipes.values_list('recipes', flat=True).distinct()
        if ingredients: 
            print 'array', ingredients_array
            condicion = condicion & Q(id__in=recipes_values)
        
            
        print condicion   
        recipes = Recipes.objects.filter(condicion)
        
        if not recipes:
            notfound=RecipesNotFound(name=name, ingredientes=ingredients)
            notfound.save()
            data["country"] = Country.objects.all()
            data["food"] = Food.objects.all()
            data["category"] = Category.objects.all()
            data["recipes_random"] = Recipes.objects.order_by('?')[:5]
            data["ingredients"] = Ingredients.objects.all()
            data["recipes_home"] = Recipes.objects.filter(home=True)[:6]
        
        
            data['notfound']='Receta no encontrada, evaluaremos su futura incorporacion. Por favor suscribete para enviarte nuestras recetas incorporadas.'
            return render_to_response(
                template_name, 
                data, 
                context_instance=RequestContext(request)
            )
        #print recipes
        
        data["country"] = Country.objects.all()
        data["food"] = Food.objects.all()
        data["category"] = Category.objects.all()
        data["recipes_random"] = Recipes.objects.order_by('?')[:5]
        data["ingredients"] = Ingredients.objects.all()
        data["recipes_home"] = Recipes.objects.filter(home=True)[:6]
        data["recipes"]=[]
        data["post"]=True
        for i in recipes: 
            if ingredients:
                active= IngredientsRecipes.objects.filter(recipes=i, ingredients__name__in = ingredients_array).exclude(ingredients__isnull=True).values('ingredients__name').distinct()
                inactive = IngredientsRecipes.objects.filter(recipes=i).exclude(ingredients__name__in = ingredients_array).exclude(ingredients__isnull=True).values('ingredients__name').distinct()
                
                data["recipes"]=[{'recipe_object':i, 'ingredients_active': active, 'ingredients_inactive': inactive , 'cantidad': active.count() }]+data["recipes"]
            else:      
                active=IngredientsRecipes.objects.filter(recipes=i).exclude(ingredients__isnull=True).values('ingredients__name').distinct()
                
                data["recipes"]=[{'recipe_object':i, 'ingredients_active': active, 'ingredients_inactive': [] , 'cantidad': active.count()}]+data["recipes"]
            

        init= page*10
        data["recipes"] = sorted(data["recipes"], key=lambda k: k['cantidad'], reverse=True)
        data_save = data
        request.session['recipes']= data_save  
         
        #print '---->', data_save

        
        paginator = Paginator(data["recipes"], 10)
        data["recipes"]= paginator.page(1)
        request.session['paginador']= paginator 
        #print data["recipes"]
        #count=data["recipes"].count()
        #data["recipes"] = data["recipes"][init:init+10]
        #i=0
        #j=0
        #pages=[]
        #print count
        #while i<count:
        #    pages.push(j)
        #    j = j+1
        #    i= i+10
        return render_to_response(
            template_name, 
            data, 
		context_instance=RequestContext(request))
        


def detalle(request, recetas_id):
    template_name = 'detalle.html'
    recipe = Recipes.objects.get(id=recetas_id)
    ingredients = IngredientsRecipes.objects.filter(recipes=recipe)
    data={'recipe':recipe, 'ingredients':ingredients}
    data["recipes_random"] = Recipes.objects.order_by('?')[:5]
    data["header"]='home'
    return render_to_response(
		template_name, 
		data, 
		context_instance=RequestContext(request))


def tips(request):
    template_name = 'tips.html'
    data={}
    data["tips"] = Tips.objects.all()
    data["recipes_random"] = Recipes.objects.order_by('?')[:5]
    data["header"]='tips'
    return render_to_response(
        template_name, 
        data, 
		context_instance=RequestContext(request))
        


def detalle_tips(request, tips_id):
    template_name = 'detalle_tips.html'
    tip = Tips.objects.get(id=tips_id)
    data={'tip':tip}
    data["recipes_random"] = Recipes.objects.order_by('?')[:5]
    data["header"]='tips'
    
    return render_to_response(
		template_name, 
		data, 
		context_instance=RequestContext(request))


def script(request):
    from bs4 import BeautifulSoup, Comment
    import requests
    import re
    import urllib, urlparse

    recipes=Recipes.objects.all()
    #recipes=IngredientsRecipes.objects.all()
    for i in recipes:
        try:
            #image=i.imagen_principal
            #parts = urlparse.urlsplit(image)
            #parts = parts._replace=urllib.quote(parts.path.encode('utf8')))
            #image = parts.geturl()
            #name=image.split('/')
            #total = len(name)
            #name= name[total-1]
            #aux=name.split('jpg')
            #if len(aux)>1:
            #    name = aux[0]+'jpg'
            
            #link = 'https://dl.dropboxusercontent.com/u/5657408/recitips/'+name
            #i.imagen_principal = link
            #i.save()
            #urllib.urlretrieve(image, name
            #-----------cambiar procediemiento
            content = i.content
            i.content = content.replace(u'KUKY®', "")
            i.save()
            #---cambiar ingredientes
            #cantidad = iantidad
            #i.cantidad = cantidad.replace(u'KUKY®', "")
            #i.save()
        except:
            print 'no salvada', image
            continue

    return HttpResponse("listo")      

def script2(request):

    from bs4 import BeautifulSoup, Comment
    import requests
    import re
    #entradas: 10 page
    #ensaladas: 5 page
    #arroces y cereales: 5 page
    #aver y caza: 3 paginas
    #carne: 3 page
    #mariscos: 3page
    #pastas: 3page
    #pesacado: 3page
    #verduras: 3 page
    #legumbres: 3page
    #salsas: 1 page
    #sopas: 2 page
    #guisos: 1 page
    url = "http://www.recetasgratis.net/Recetas-de-Guisos-Potage-listado_receta-19_1.html"

    req = requests.get(url)

    # Comprobamos que la peticin nos devuelve un Status Code = 200
    statusCode = req.status_code
    if statusCode == 200:

        # Pasamos el contenido HTML de la web a un objeto BeautifulSoup()
        html = BeautifulSoup(req.text)
        #articulos = html.find_all('article') 
        #print articulos

        entradas = html.find_all('div',{'class':'receta'})
        #print entradas
        for i,entrada in enumerate(entradas):
            #try:
            titulo = entrada.find('a',{'class':'titulo'}) 
            if titulo: 
                href=titulo['href']
                titulo=titulo.getText()
                imagen = entrada.find('img')['src']
                print '------------->', href, titulo, imagen
                req = requests.get(href)
                statusCode = req.status_code
                if statusCode == 200:
                    html = BeautifulSoup(req.text)
                    contenido = html.find('ol',{'class':'pasos'}) 
                    
                    
                        
                    #print contenido
                        
                    #contenido_array= contenido.split("")
                    #
                    #print contenido_array
                    #if len(contenido_array)>1:
                    #    contenido = contenido_array[0]+contenido_array[1]
                    #else: 
                    #    contenido = contenido_array[0]
                    if not contenido:
                        contenido = ""
                    else:
                        for element in contenido(text=lambda text: isinstance(text, Comment)):
                            element.extract()
                    #print 'contenido', contenido
                    valores = html.find('span',{'class':'infos'})
                    #print valores
                    [s.extract() for s in valores('small')]
                    try:
                        valores = valores.findAll('span')
                        print '--->', valores[0]
                        if 'minutos' in valores[0].getText() or 'hora' in valores[0].getText():
                            print 'No acutor'
                            duracion = valores[0].getText()
                            porciones=valores[1].getText()     
                        else:
                            duracion = valores[3].getText()
                            porciones=valores[5].getText()

                        if 'minutos' in duracion:
                                duracion=duracion.split('minutos')[0]                        
                        if 'hora' in duracion:
                                duracion=int(duracion.split('hora')[0])*60
                    except:
                        print 'no tiene informacion suficiente'
                        continue
                        
                    
                    #print porciones, duracion

                    aux_1=Recipes.objects.filter(name=titulo)
                    #print '--->rece', aux_1
                    if not aux_1:
                        recipes = Recipes(name=titulo, imagen_principal=imagen, content=str(contenido), creator=href, porciones=porciones, category_id=11, tiempo=duracion, pais_id=2)
                        recipes.save()
                        #ingredientes = html.find('p', text = re.compile('Ingredientes')) 
                        ingredientes = html.findAll('li',{'itemprop':'recipeIngredient'})
                        #print '-->', ingredientes
                        #ingredientes= ingredientes.findAll('li')
                        #print ingredientes
                        #aux=0
                        for i in ingredientes:
                            ingrediente=i.findAll('span',{'class':'ingrediente'})
                            if ingrediente:
                                ingrediente=i.find('span',{'class':'ingrediente'}).getText()
                                if i.findAll('span',{'class':'unidad'}):
                                    cantidad = i.find('span',{'class':'cantidad'}).getText() + ' '+ i.find('span',{'class':'unidad'}).getText() + ' ' + ingrediente
                                else:
                                    if i.findAll('span',{'class':'cantidad'}):
                                        cantidad = i.find('span',{'class':'cantidad'}).getText() + ' '+ ingrediente
                                    else:
                                        cantidad = ingrediente

                                print cantidad
                                print ingrediente
                            else: 
                                if i.findAll('a'):
                                    ingrediente=i.find('a').getText()
                                    cantidad = i.getText()
                                else: 
                                    cantidad = i.getText()
                                    ingrediente=''
                            
                            aux = Ingredients.objects.filter(name__iexact=ingrediente)
                            if aux:
                                ingredients=aux[0] 
                                if ingrediente=='':
                                    receta_ingrediente=IngredientsRecipes(cantidad=cantidad, recipes=recipes)
                                    receta_ingrediente.save() 
                                else:
                                    receta_ingrediente=IngredientsRecipes(cantidad=cantidad, recipes=recipes, ingredients=ingredients)
                                    receta_ingrediente.save()                   
                            else:
                                if ingrediente=='':
                                    receta_ingrediente=IngredientsRecipes(cantidad=cantidad, recipes=recipes)
                                    receta_ingrediente.save() 
                                else:
                                    ingredients= Ingredients(name=ingrediente)
                                    ingredients.save()
                                    receta_ingrediente=IngredientsRecipes(cantidad=cantidad, recipes=recipes, ingredients=ingredients)
                                    receta_ingrediente.save()

                                    
            #except:
            #    print "receta dio error:", titulo
                                
        return HttpResponse("listo")                          


                    #print "titulo, contenido", titulo, contenido

                # Imprimo el Ttulo, Autor y Fecha de las entradas
        #	print "----"


    else:
        return HttpResponse("error")  

def script_postres(request):

    from bs4 import BeautifulSoup, Comment
    import requests
    import re

    url = "https://www.dulcereceta.cl/recetas/pagina/21/"

    req = requests.get(url)

    # Comprobamos que la peticin nos devuelve un Status Code = 200
    statusCode = req.status_code
    if statusCode == 200:

        # Pasamos el contenido HTML de la web a un objeto BeautifulSoup()
        html = BeautifulSoup(req.text)
        #articulos = html.find_all('article') 
        #print articulos

        entradas = html.find_all('article',{'class':'recipe-box'})
        #print entradas
        for i,entrada in enumerate(entradas):
            #try:
            titulo = entrada.find('a',{'class':'normal-title'}) 
            if titulo: 
                href=titulo['href']
                titulo=titulo.getText()
                imagen = entrada.find('img')['src']
                #print '------------->', href, titulo, imagen
                req = requests.get(href)
                statusCode = req.status_code
                if statusCode == 200:
                    html = BeautifulSoup(req.text)
                    contenido = html.find('div',{'class':'see-more-wrapp'}) 
                    
                    for element in contenido(text=lambda text: isinstance(text, Comment)):
                        element.extract()
                        
                    #print contenido
                        
                    #contenido_array= contenido.split("")
                    #
                    #print contenido_array
                    #if len(contenido_array)>1:
                    #    contenido = contenido_array[0]+contenido_array[1]
                    #else: 
                    #    contenido = contenido_array[0]
                    if not contenido:
                        contenido = ""
                    #print 'contenido', contenido
                    valores = html.findAll('p',{'class':'recipe-prop-value'})
                    duracion=valores[0].getText()
                    porciones=valores[2].getText()
                    #print porciones, duracion

                    aux_1=Recipes.objects.filter(name=titulo)
                    print '--->rece', aux_1
                    if not aux_1:
                        #print contenido
                        if 'minutos' in duracion:
                            duracion=duracion.split('minutos')[0]                        
                        if 'hora' in duracion:
                            duracion=duracion.split('hora')[0]
                        recipes = Recipes(name=titulo, imagen_principal=imagen, content=str(contenido), creator=href, porciones=porciones, category_id=2, tiempo=duracion, pais_id=2)
                        recipes.save()
                        #ingredientes = html.find('p', text = re.compile('Ingredientes')) 
                        ingredientes = html.find('ul',{'class':'ingredients-list'})
                        print '-->', ingredientes
                        ingredientes= ingredientes.findAll('li')
                        print ingredientes
                        aux=0
                        for i in ingredientes:
                            ingrediente=i.getText()
                            receta_ingrediente=IngredientsRecipes(cantidad=ingrediente, recipes=recipes, ingredients_id=1)
                            receta_ingrediente.save()
                                    
            #except:
            #    print "receta dio error:", titulo
                                
        return HttpResponse("listo")                          


                    #print "titulo, contenido", titulo, contenido

                # Imprimo el Ttulo, Autor y Fecha de las entradas
        #	print "----"


    else:
        print "Status Code %d" %statusCode
        
def script_tragos(request):

    from bs4 import BeautifulSoup
    import requests
    import re

    url = "http://www.lostragos.com/categoria/clasicos/"

    req = requests.get(url)

    # Comprobamos que la peticin nos devuelve un Status Code = 200
    statusCode = req.status_code
    if statusCode == 200:

        # Pasamos el contenido HTML de la web a un objeto BeautifulSoup()
        html = BeautifulSoup(req.text)
        #articulos = html.find_all('article') 
        #print articulos


        # Obtenemos todos los divs donde estan las entradas
        entradas = html.find_all('article')
        # Recorremos todas las entradas para extraer el ttulo, autor y fecha
        for i,entrada in enumerate(entradas):
        # Con el mtodo "getText()" no nos devuelve el HTML
            try:
                titulo = entrada.find('h1',{'class':'entry-title'}) 
                if titulo: 
                    href=titulo.find('a')['href']
                    titulo=titulo.find('a').getText()
                    imagen = entrada.find('img')['src']
                    req = requests.get(href)
                    statusCode = req.status_code
                    if statusCode == 200:
                        html = BeautifulSoup(req.text)
                        contenido = html.find('div',{'class':'receta-content'}) 
                        if not contenido:
                            contenido = html.find('div',{'class':'entry-content'})

                        aux_1=Recipes.objects.filter(name=titulo)
                        if not aux_1:
                            #print contenido
                            recipes = Recipes(name=titulo, imagen_principal=imagen, content=str(contenido), creator=href, porciones=1, category_id=4, tiempo=10, pais_id=2)
                            recipes.save()
                            ingredientes = html.find('p', text = re.compile('Ingredientes')) 
                            aux=0
                            while True:
                                ingredientes=ingredientes.findNext('p')
                                #print ingredientes
                                span = ingredientes.findAll('span')

                                if u'Preparaci' in ingredientes.getText():
                                    break
                                if len(span)>0:
                                    cantidad = span[0].getText()+span[1].getText()
                                    ingrediente = span[2].getText()
                                    #print '--->', cantidad, ingrediente
                                    aux = Ingredients.objects.filter(name__iexact=ingrediente)
                                    if aux:
                                        ingredients=aux[0] 
                                        receta_ingrediente=IngredientsRecipes(cantidad=cantidad, recipes=recipes, ingredients=ingredients)
                                        receta_ingrediente.save()                   
                                    else:
                                        ingredients= Ingredients(name=ingrediente)
                                        ingredients.save()
                                        receta_ingrediente=IngredientsRecipes(cantidad=cantidad, recipes=recipes, ingredients=ingredients)
                                        receta_ingrediente.save()
                                else:
                                    cantidad = ingredientes.getText()
                                    receta_ingrediente=IngredientsRecipes(cantidad=cantidad, recipes=recipes, ingredients_id=1)
                                    receta_ingrediente.save()
            except:
                print "receta dio error:", titulo
                                
        return HttpResponse("listo")                          


                    #print "titulo, contenido", titulo, contenido

                # Imprimo el Ttulo, Autor y Fecha de las entradas
        #	print "----"


    else:
        print "Status Code %d" %statusCode
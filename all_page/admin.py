from django.contrib import admin
from django import forms
from django.contrib.admin.helpers import ActionForm

from .models import Ingredients, Recipes, IngredientsRecipes, Country, Food, Category, Tips, UserMail, RecipesNotFound


class IngredientsInline(admin.TabularInline):
    model = IngredientsRecipes

class RecipesAdmin(admin.ModelAdmin):
    inlines = [IngredientsInline,]
    search_fields = ('name', 'id')
    list_filter=('category__name', 'pais__name', 'light', 'home')

    
def combinar(modeladmin, request, queryset):
    for obj in queryset:
        ingrediente_id = request.POST['original']
        print ingrediente_id
        IngredientsRecipes.objects.filter(ingredients__id=obj.id).update(ingredients=Ingredients.objects.get(id=ingrediente_id))
        obj.delete()
        
class XForm(ActionForm):
    original = forms.CharField(required = False)
    
class IngredientsAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    search_fields = ('name', 'id')
    action_form = XForm
    actions = [combinar]
    

    
    
    
    

admin.site.register(Ingredients, IngredientsAdmin)
admin.site.register(Recipes, RecipesAdmin)
#admin.site.register(Recipes)
admin.site.register(IngredientsRecipes)
admin.site.register(Country)
admin.site.register(Food)
admin.site.register(Category)
admin.site.register(Tips)
admin.site.register(UserMail)
admin.site.register(RecipesNotFound)

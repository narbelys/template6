from django.db import models

from django.conf import settings
from django.contrib.auth.models import User
from tinymce.models import HTMLField

class Ingredients(models.Model):
    name = models.CharField(max_length=200)
    
    def __unicode__(self):
        return self.name
    class Meta:
        ordering = ['name']
    
class Recipes(models.Model):
    name = models.CharField(max_length=200)
    imagen_principal = models.CharField(max_length=1500, null=True, blank=True)
    content = HTMLField()
    #content = models.TextField(max_length=40000, blank=True)
    creator = models.CharField(max_length=1500, null=True, blank=True)
    porciones = models.IntegerField(default=1)
    pais = models.ForeignKey('Country', null=True, blank=True)
    plato = models.ForeignKey('Food', null=True, blank=True)
    category = models.ForeignKey('Category', null=True, blank=True)
    tiempo = models.IntegerField(default=1, null=True, blank=True)
    light = models.IntegerField(default=0, null=True, blank=True)
    home = models.IntegerField(default=0, null=True, blank=True)
    
    def __unicode__(self):
        return self.name
    
class IngredientsRecipes(models.Model):
    cantidad = models.CharField(max_length=1500, blank=True) #ingrediente
    recipes = models.ForeignKey('Recipes')
    ingredients = models.ForeignKey('Ingredients', blank=True, null=True)
    
    def __unicode__(self):
        if self.ingredients:
            return self.recipes.name + self.ingredients.name
        else:
            return self.recipes.name
    
class Country(models.Model):
    name = models.CharField(max_length=1500)
    def __unicode__(self):
        return self.name
    
class Food(models.Model):
    name = models.CharField(max_length=1500)
    def __unicode__(self):
        return self.name
    
class Category(models.Model):
    name = models.CharField(max_length=1500)
    def __unicode__(self):
        return self.name
    
class Tips(models.Model):
    name = models.CharField(max_length=200)
    imagen = models.CharField(max_length=1500, null=True, blank=True)
    description = models.CharField(max_length=1500, null=True, blank=True)
    content = models.TextField(max_length=40000)
    creator = models.CharField(max_length=1500, null=True, blank=True)
    
    def __unicode__(self):
        return self.name
    
    
class UserMail(models.Model):
    name = models.CharField(max_length=200, null=True, blank=True)
    correo = models.CharField(max_length=200, null=True, blank=True)
    mensaje = models.CharField(max_length=1500, null=True, blank=True)
    
    def __unicode__(self):
        return self.correo
    class Meta:
        ordering = ['correo']
        
        
class RecipesNotFound(models.Model):
    name = models.CharField(max_length=1500, null=True, blank=True)
    ingredientes = models.CharField(max_length=1500, null=True, blank=True)
    
    def __unicode__(self):
        return self.name
    class Meta:
        ordering = ['name']
